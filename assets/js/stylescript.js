// == TOOGLE MENU SIDE 
const toggle_menu = document.getElementById('toggle-menu');

toggle_menu.addEventListener('click' , function() {

    var content = document.getElementById('left-side');
    const styles = getComputedStyle(content);

    if(styles.display === 'none'){

        content.style.display = 'block';
        content.classList.remove('left-side-hide');
        content.style.animationName = 'leftsideshow';
        content.style.animationPlayState = 'running';

    }else{

        if(content.classList.contains('left-side-hide')){
            content.classList.remove('left-side-hide');
            content.style.animationName = 'leftsideshow';
            content.style.animationPlayState = 'running';
        }else{
            content.classList.add('left-side-hide');
            content.style.animationName = 'leftsidehide';
            content.style.animationPlayState = 'running';
        }

    }   
    
});

// MENU LEVEL
var togglemenulevel = document.getElementsByClassName('toggle-menu-level');
var i;

for (i =0 ; i < togglemenulevel.length; i++){
    togglemenulevel[i].addEventListener("click" , function(){
        this.classList.toggle('active');
        var content = this.nextElementSibling;
        var styles = getComputedStyle(content);
        if(styles.display === 'none'){
             content.style.display = 'block';
        }else{
            content.style.display = 'none';
        }
    });
}
